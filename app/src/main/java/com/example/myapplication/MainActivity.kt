package com.example.myapplication

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.view.View
import kotlinx.android.synthetic.main.activity_main.*
import java.lang.Exception

class MainActivity : AppCompatActivity(), View.OnClickListener {


    override fun onClick(p0: View?) {
        when(p0!!.id){
            R.id.btn_calcular ->{
                try {
                    var resul1 =
                        uno.text.toString().toInt() + dos.text.toString().toInt() + tres.text.toString().toInt()
                    var resul2 =
                        cuatro.text.toString().toInt() + cinco.text.toString().toInt() + seis.text.toString().toInt()
                    var resul3 =
                        siete.text.toString().toInt() + ocho.text.toString().toInt() + nueve.text.toString().toInt()

                    var resul4 =
                        uno.text.toString().toInt() + cuatro.text.toString().toInt() + siete.text.toString().toInt()
                    var resul5 =
                        dos.text.toString().toInt() + cinco.text.toString().toInt() + ocho.text.toString().toInt()
                    var resul6 =
                        tres.text.toString().toInt() + seis.text.toString().toInt() + nueve.text.toString().toInt()

                    var resul7 =
                        uno.text.toString().toInt() + cinco.text.toString().toInt() + nueve.text.toString().toInt()

                    panel1.setText(resul4.toString())
                    panel2.setText(resul5.toString())
                    panel3.setText(resul6.toString())
                    panel4.setText(resul7.toString())

                    panel5.setText(resul3.toString())
                    panel6.setText(resul2.toString())
                    panel7.setText(resul1.toString())


                    if (
                        resul2 == resul1 &&
                        resul3 == resul2 &&
                        resul4 == resul3 &&
                        resul5 == resul4 &&
                        resul6 == resul5 &&
                        resul7 == resul6
                    ) {

                        panelPrincipal.setText(R.string.txt_ganador)

                    } else {
                        panelPrincipal.setText(R.string.txt_perdedor)
                    }
                }catch (ex:Exception){

                    panelPrincipal.setText(R.string.txt_novalidos)
                }
            }

            R.id.btn_limpiar ->{
                uno.setText("")
                dos.setText("")
                tres.setText("")
                cuatro.setText("")
                cinco.setText("")
                seis.setText("")
                siete.setText("")
                ocho.setText("")
                nueve.setText("")

                panel1.setText("0")
                panel2.setText("0")
                panel3.setText("0")
                panel4.setText("0")
                panel5.setText("0")
                panel6.setText("0")
                panel7.setText("0")


                panelPrincipal.setText(R.string.txt_panelPrincipal)
            }
        }
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        btn_calcular.setOnClickListener(this)
        btn_limpiar.setOnClickListener(this)
    }
}
